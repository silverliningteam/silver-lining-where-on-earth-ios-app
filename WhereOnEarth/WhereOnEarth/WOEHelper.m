//
//  WOEHelper.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import "WOEHelper.h"
#define LOGIN @"userIsLogin"
#define FBID @"FBID"
@implementation WOEHelper
+(BOOL)isLogin{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
   return [[ud objectForKey:LOGIN] boolValue];
}
+(void)setLogin{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud setValue:[[NSNumber alloc] initWithBool:YES] forKey:LOGIN];
    [ud synchronize];
}
+(void)setUserFbId:(NSString *)fbId{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud setValue:fbId forKey:FBID];
    [ud synchronize];
}
+(NSString *)fbId{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    return [ud objectForKey:FBID];
}
@end
