//
//  AppDelegate.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
