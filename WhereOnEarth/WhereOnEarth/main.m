//
//  main.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
