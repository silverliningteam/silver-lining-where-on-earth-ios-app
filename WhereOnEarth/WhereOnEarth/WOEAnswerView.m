//
//  WOEAnswerView.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import "WOEAnswerView.h"

@implementation WOEAnswerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(id)initWithXibName:(NSString *)xibName andPosition:(CGPoint)pos{
    self = [super initWithXibName:xibName andPosition:pos];
    return self;
}
-(void)hideViewToPos:(CGPoint)pos{
    [super hideViewToPos:pos];
    [_cityTextField resignFirstResponder];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
