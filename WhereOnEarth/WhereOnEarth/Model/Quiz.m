//
//  Quiz.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import "Quiz.h"

@implementation Quiz
-(NSMutableArray *)hints{
    if (!_hints) {
        _hints = [[NSMutableArray alloc] init];
    }
    return _hints;
}
-(NSString *)attempts{
    if (!_attempts) {
        return @"";
    }
    return _attempts;
}
-(NSString *)image{
    if (!_image) {
        return @"";
    }
    return _image;
}
@end
