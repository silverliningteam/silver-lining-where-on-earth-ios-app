//
//  Quiz.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import <Foundation/Foundation.h>

@interface Quiz : NSObject
@property (nonatomic,retain) NSMutableArray * hints;
@property (nonatomic,retain) NSString * attempts;
@property (nonatomic,retain) NSString * image;
@end
