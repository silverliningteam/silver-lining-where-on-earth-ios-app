//
//  WOEHelper.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import <Foundation/Foundation.h>

@interface WOEHelper : NSObject
+(BOOL)isLogin;
+(void)setLogin;
+(void)setUserFbId:(NSString*)fbId;
+(NSString*)fbId;
@end
