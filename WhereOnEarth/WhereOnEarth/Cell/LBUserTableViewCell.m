//
//  LBUserTableViewCell.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import "LBUserTableViewCell.h"
#define NUMBERCOLOR [[UIColor alloc] initWithRed:255.0f/255.0f green:168.0/255.0f blue:0/255.0f alpha:1]

@implementation LBUserTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configureCellWithUserName:(NSString *)name withNumberOfUserSolvedQuiz:(NSInteger)numOfSolvedQuizuserRank{
    self.username.text = name;
    NSMutableAttributedString * attemptText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:ATTEMPT_CONTEXT,numOfSolvedQuizuserRank] attributes:@{[UIColor blackColor]:NSForegroundColorAttributeName}];
    [attemptText addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(6,[NSString stringWithFormat:@"%d",numOfSolvedQuizuserRank].length)];
    self.attempt.attributedText = attemptText;
}
@end
