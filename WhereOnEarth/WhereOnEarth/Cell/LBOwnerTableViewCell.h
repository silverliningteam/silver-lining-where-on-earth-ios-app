//
//  LBOwnerTableViewCell.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import "LBUserTableViewCell.h"

@interface LBOwnerTableViewCell : LBUserTableViewCell
@property (weak, nonatomic) IBOutlet UIView *highlightView;
@property (weak, nonatomic) IBOutlet UILabel *owerRank;

@end
