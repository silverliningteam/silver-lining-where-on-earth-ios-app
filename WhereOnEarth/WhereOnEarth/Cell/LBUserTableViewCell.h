//
//  LBUserTableViewCell.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import <UIKit/UIKit.h>
#define ATTEMPT_CONTEXT @"KNOWS %d PLACES"
@interface LBUserTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UILabel *attempt;
@property (weak, nonatomic) IBOutlet UIImageView *avater;
-(void)configureCellWithUserName: (NSString*)name withNumberOfUserSolvedQuiz: (NSInteger)numOfSolvedQuiz;
@end
