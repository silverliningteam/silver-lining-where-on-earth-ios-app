//
//  hintsCell.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import <UIKit/UIKit.h>

@interface hintsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *hintImageView;

@end
