//
//  WOEOtherUserSuccessView.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import "WOEOtherUserSuccessView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define CONTEXT @"%@ SOLVED THIS ONE! NEXT QUESTION IN %d SECONDS"
#define NUMBERCOLOR [[UIColor alloc] initWithRed:255.0f/255.0f green:168.0/255.0f blue:0/255.0f alpha:1]

@implementation WOEOtherUserSuccessView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)updateWithAvatar:(NSString *)url andTitle:(NSString *)title andTime:(NSInteger)time{
    NSMutableAttributedString * context = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:CONTEXT,title,time] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [context addAttribute:NSForegroundColorAttributeName value:NUMBERCOLOR range:NSMakeRange(35+title.length,time>9?2:1)];
    [context addAttribute:NSForegroundColorAttributeName value:NUMBERCOLOR range:NSMakeRange(0, title.length)];
    [self.title setAttributedText:context];
    [self.avatar setImageWithURL:[[NSURL alloc] initWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {}];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
