//
//  WOESuccessView.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import "WOEBaseView.h"

@interface WOESuccessView : WOEBaseView
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *shareFacebookBtn;
-(void)updateTitle:(NSInteger) title;
@end
