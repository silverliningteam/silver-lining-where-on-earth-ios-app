//
//  WOEBaseView.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import "WOEBaseView.h"

@implementation WOEBaseView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(id)initWithXibName:(NSString *)xibName andPosition:(CGPoint)pos{
    self = [[[NSBundle mainBundle] loadNibNamed:xibName owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.center = pos;
        self.alpha = 0;
    }
    return self;
}
-(void)moveViewTo:(CGPoint)pos andAlpha: (int)alpha{
    [UIView animateWithDuration:1 animations:^{
        self.alpha = alpha;
        self.center = pos;
    }];
}
-(void)showViewToPos:(CGPoint)pos{
    
    [self moveViewTo:pos andAlpha:1];
}
-(void)hideViewToPos:(CGPoint)pos{
    [self moveViewTo:pos andAlpha:0];
}
@end
