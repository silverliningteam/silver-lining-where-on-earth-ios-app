//
//  WOEOtherUserSuccessView.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import "WOEBaseView.h"
@interface WOEOtherUserSuccessView : WOEBaseView
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *title;
-(void)updateWithAvatar:(NSString*)url andTitle: (NSString*)title andTime:(NSInteger)time;
@end
