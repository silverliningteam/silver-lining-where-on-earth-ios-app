//
//  WOEBaseView.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import <UIKit/UIKit.h>

@interface WOEBaseView : UIView
-(id)initWithXibName: (NSString*)xibName andPosition: (CGPoint)pos;
-(void)showViewToPos:(CGPoint)pos;
-(void)hideViewToPos:(CGPoint)pos;
@end
