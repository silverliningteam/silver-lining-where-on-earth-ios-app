//
//  WOESuccessView.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import "WOESuccessView.h"
#define CONTEXT @"YOU GOT THE CORRECT ANSWER! NEXT QUESTION IN %d SECOND"
#define NUMBERCOLOR [[UIColor alloc] initWithRed:255.0f/255.0f green:168.0/255.0f blue:0/255.0f alpha:1]
@implementation WOESuccessView

-(void)updateTitle:(NSInteger)title{
    NSMutableAttributedString * content = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:CONTEXT,title] attributes:@{[UIColor whiteColor]:NSForegroundColorAttributeName}];
    [content addAttribute:NSForegroundColorAttributeName value:NUMBERCOLOR range:NSMakeRange(45,title>9?2:1)];

    [_title setAttributedText:content];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
