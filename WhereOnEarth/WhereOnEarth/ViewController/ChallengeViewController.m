//
//  ViewController.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//
#import <QuartzCore/QuartzCore.h>
#import "ChallengeViewController.h"
#import "WOESuccessView.h"
#import "WOEFailView.h"
#import "WOEAnswerView.h"
#import "WOEPicker.h"
#import "hintsCell.h"
#import "WOEOtherUserSuccessView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define GET_CURRENT_QUIZ @"get_current_quiz"
#define CURRENT_QUIZ @"current_quiz"
#define NEW_HINTS @"new_hint"
#define NEW_QUIZ @"new_quiz"
#define ARGS @"args"
#import "Quiz.h"

@interface ChallengeViewController () <UIPickerViewAccessibilityDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
- (IBAction)historyButtonPressed:(id)sender;
- (IBAction)leaderBoardButtonPressed:(id)sender;
- (IBAction)answerButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView  * collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *quizImageView;
@property (retain,nonatomic) UIView * dimView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (retain,nonatomic) WOESuccessView * successView;
@property (retain,nonatomic) WOEFailView * failView;
@property (retain,nonatomic) WOEAnswerView * answerView;
@property (retain,nonatomic) UIPickerView * countryPicker;
@property (nonatomic,retain) NSArray * countries;
@property (weak, nonatomic) IBOutlet UILabel *attemptLabel;
@property (nonatomic,getter = isShowingPicker) BOOL showingPicker;
@property(nonatomic,retain) WOEOtherUserSuccessView * otherUserSuccessView;
@property NSInteger changeTick;
@property (nonatomic,retain) Quiz * currentQuiz;
@property (nonatomic,retain) SocketIO * socketIO;
@property int countryPos;
@end

@implementation ChallengeViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpData];
    [self setUpUI];
    _countryPos = -1;
    [self.socketIO sendEvent:@"get_current_quiz" withData:nil];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpData{
    _changeTick = 10;
}
-(void)setUpUI{
//    _backgroundImageView.layer.zPosition = 1;
    _quizImageView.layer.shadowColor = [UIColor whiteColor].CGColor;
    _quizImageView.layer.shadowOffset = CGSizeMake(0, 1);
    _quizImageView.layer.shadowOpacity = 1;
    _quizImageView.layer.shadowRadius = 5.0;
    _quizImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _quizImageView.layer.borderWidth = 3.0;
    _quizImageView.layer.cornerRadius = 10.0;
    self.view.layer.zPosition = 1;
    [self.view addSubview:self.dimView];
    self.dimView.alpha = 0;
    [self.view addSubview:self.successView];
    [self.view addSubview:self.failView];
    [self.view addSubview:self.answerView];
    [self.view addSubview:self.countryPicker];
    [self.view addSubview:self.otherUserSuccessView];
    //convert object to data
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"list_countries" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    _countries = json[@"countries"];
    
}
-(Quiz *)currentQuiz{
    if (!_currentQuiz) {
        _currentQuiz = [[Quiz alloc] init];
    }
    return _currentQuiz;
}
#pragma mark - Socket io

-(SocketIO *)socketIO{
    if (!_socketIO) {
        _socketIO = [[SocketIO alloc] initWithDelegate:self];
        // connect to the socket.io server that is running locally at port 3000
        [_socketIO connectToHost:@"192.168.0.102" onPort:8080];
    }
    return _socketIO;
}
- (void) socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
}
-(void)socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet{
    NSDictionary * msg  = packet.dataAsJSON;
    
    NSLog(@"didReceiveMessage >>> data: %@", msg);
    NSString * cmd  = msg[@"name"];
    if ([cmd isEqualToString:CURRENT_QUIZ]) {
        _currentQuiz.hints = msg[ARGS][0][@"hints"];
        _currentQuiz.attempts = msg[ARGS][0][@"attempts"];
        _currentQuiz.image = msg[ARGS][0][@"image"];
        [_quizImageView setImageWithURL:[NSURL URLWithString:_currentQuiz.image] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        }];
        [_attemptLabel setText:[NSString stringWithFormat:@"%@",_currentQuiz.attempts]];
        
        [_collectionView reloadData];
        [self.view setNeedsDisplay];
    }else if ([cmd isEqualToString:NEW_HINTS]){
        if (_currentQuiz.hints.count <7){
            NSDictionary * image = msg[ARGS][0];
            NSLog(@"%@ \n %@",_currentQuiz.hints,image);
            [_currentQuiz.hints addObject:image];
            [_collectionView reloadData];
        }
    }else if([cmd isEqualToString:@"attempt_success"]){
        [self showDimView];
        [_successView showViewToPos:CGPointMake(160, 200)];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_changeTick * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_successView hideViewToPos:[self positionAlertView]];
            [self showDimView];
        });
    }else if ([cmd isEqualToString:@"attempt_fail"]){
        [_failView showViewToPos:CGPointMake(160, 200)];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_failView hideViewToPos:[self positionAlertView]];
        });
    }
//    }else if ([cmd isEqualToString:])
        
}
- (void) socketIO:(SocketIO *)socket onError:(NSError *)error
{
    if ([error code] == SocketIOUnauthorized) {
        NSLog(@"not authorized");
    } else {
        NSLog(@"onError() %@", error);
    }
}
- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
}

#pragma mark - UI
-(CGPoint)positionAlertView{
    return CGPointMake(160, self.view.frame.size.height+100);
}
-(UIView*)dimView{
    if (!_dimView) {
        _dimView = [[UIView alloc] initWithFrame:self.view.frame];
        _dimView.backgroundColor = [UIColor blackColor];
        _dimView.layer.zPosition = 2;
        [_dimView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnDimView:)]];
    }
    return _dimView;
}

-(WOESuccessView *)successView{
    if (!_successView) {
        _successView =  [[WOESuccessView alloc] initWithXibName:@"Success" andPosition:[self positionAlertView]];
        [_successView updateTitle:_changeTick];
        _successView.layer.zPosition = 3;
    }
    return _successView;
}
-(WOEFailView *)failView{
    if (!_failView) {
        _failView = [[WOEFailView alloc] initWithXibName:@"wrong_answer" andPosition:[self positionAlertView]];
        _failView.layer.zPosition = 3;
    }
    return _failView;
}
-(WOEAnswerView *)answerView{
    if (!_answerView) {
        _answerView = [[WOEAnswerView alloc] initWithXibName:@"answer" andPosition:[self positionAlertView]];
        _answerView.cityTextField.delegate=self;
        [_answerView.countryBtn addTarget:self action:@selector(countryAnswerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_answerView.submitBtn addTarget:self action:@selector(submitPressed:) forControlEvents:UIControlEventTouchUpInside];
        _answerView.layer.zPosition = 3;
    }
    return _answerView;
}

-(NSArray *)countries{
    if (!_countries) {
        _countries  = [[NSArray alloc] initWithObjects:@"HCM",@"Ha Noi",@"Da Lat", nil];
    }
    return _countries;
}
-(UIPickerView *)countryPicker{
    if (!_countryPicker) {
        _countryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height+100, self.view.frame.size.width,200)];
        _countryPicker.delegate = self;
        _countryPicker.dataSource = self;
        _countryPicker.center = [self positionAlertView];
        _countryPicker.layer.zPosition = 4;
        _countryPicker.backgroundColor=[UIColor whiteColor];
    }
    return _countryPicker;
}
-(WOEOtherUserSuccessView *)otherUserSuccessView{
    if (!_otherUserSuccessView) {
        _otherUserSuccessView = [[WOEOtherUserSuccessView alloc] initWithXibName:@"OtherUserSuccess" andPosition:[self positionAlertView]];
        [_otherUserSuccessView updateWithAvatar:@"" andTitle:@"Duy Phuc" andTime:self.changeTick];
    }
    return _otherUserSuccessView;
}
#pragma mark - Alert view
-(void)tapOnDimView:(id)sender{
    [self hideCountryPicker];
    [self hideDimView];
    [self hiddenAllAlertView];
}

-(void)showDimView{
    [UIView animateWithDuration:0.5 animations:^{
        _dimView.alpha = 0.7;
    }];
}
-(void)hideDimView{
    [UIView animateWithDuration:0.5 animations:^{
        _dimView.alpha = 0;
    }];
}
-(void)hiddenAllAlertView{
    [_successView hideViewToPos:[self positionAlertView]];
    [_failView hideViewToPos:[self positionAlertView]];
    [_answerView hideViewToPos:[self positionAlertView]];
    [_otherUserSuccessView hideViewToPos:[self positionAlertView]];
}

#pragma mark - Action
-(void)submitPressed:(id)sender{
    [self hideDimView];
    [self hiddenAllAlertView];
    [self hideCountryPicker];
    NSString * country = @"";
    if (_countryPos != -1 && _countryPos<self.countries.count) {
        country = self.countries[self.countryPos][@"name"];
    }
    NSDictionary * message  = @{@"city":_answerView.cityTextField.text,@"country_name":country};
    [self.socketIO sendEvent:@"attempt" withData:message];
}
- (IBAction)historyButtonPressed:(id)sender {
//    [self showDimView];
//    [_failView showViewToPos:CGPointMake(160, 200)];
}

- (IBAction)leaderBoardButtonPressed:(id)sender {
    [_otherUserSuccessView showViewToPos:CGPointMake(160, 200)];
}

- (IBAction)answerButtonPressed:(id)sender {
    [self showDimView];
    [_answerView showViewToPos:CGPointMake(160, 200)];
}
-(void)showCountryPicker{
    if (!self.showingPicker) {
        [UIView animateWithDuration:0.5 animations:^{
            self.showingPicker = true;
            _countryPicker.center = CGPointMake(160, 370);
        }];
    }
}
-(void)hideCountryPicker{
    if (self.showingPicker) {
        [UIView animateWithDuration:0.5 animations:^{
            self.showingPicker = false;
            _countryPicker.center = [self positionAlertView];
        }];
    }
}
-(void)countryAnswerButtonPressed:(id)sender{
    if (self.showingPicker) {
        [_answerView.cityTextField resignFirstResponder];
        [self hideCountryPicker];
    }else{
        [_answerView.cityTextField resignFirstResponder];
        [self showCountryPicker];
    }
}
#pragma mark - UICollecitonView 
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 6;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifer = @"Cell";
    hintsCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifer forIndexPath:indexPath];
    if (self.currentQuiz.hints.count>0 && indexPath.row < self.currentQuiz.hints.count) {
        NSLog(@"%@",self.currentQuiz.hints[indexPath.row][@"image"]);
        [cell.hintImageView setImageWithURL:[NSURL URLWithString:self.currentQuiz.hints[indexPath.row][@"image"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        }];
        return cell;
    }
    
    return cell;
}
#pragma mark - UIPickerView
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.countries.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.countries[row][@"name"];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [_answerView.countryBtn setTitle:self.countries[row][@"name"] forState:UIControlStateNormal];
    [self hideCountryPicker];
}
@end
