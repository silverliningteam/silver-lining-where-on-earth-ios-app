//
//  RankingTableViewController.m
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import "RankingTableViewController.h"
#import "LBOwnerTableViewCell.h"
#import "LBUserTableViewCell.h"
#import "HeaderTableViewCell.h"
#import "LBFooterTableViewCell.h"
@interface RankingTableViewController ()
@end

@implementation RankingTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"challenge_bg.png"]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return section == 1?5:4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * cellIdentifer = indexPath.row == 0?@"HeaderCell":@"Cell";
    if (indexPath.row == 0) {
        HeaderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifer forIndexPath:indexPath];
        cell.headerTitle.text = indexPath.section == 0? @"WORLD RANKING":@"FRIEND RANKING";
        return cell;
    }else{
        if (indexPath.row == 4 && indexPath.section == 1) {
            LBFooterTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FooterCell" forIndexPath:indexPath];
            return cell;
        }else{
    LBUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifer forIndexPath:indexPath];
    [cell configureCellWithUserName:[NSString stringWithFormat:@"%d. %@",1,@"Duy Phuc"] withNumberOfUserSolvedQuiz:30];
    // Configure the cell...
        return cell;
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 40;
    }
    return 60;
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
