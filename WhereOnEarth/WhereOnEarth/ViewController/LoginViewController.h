//
//  LoginViewController.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "SocketIOPacket.h"
#import "SocketIO.h"
@interface LoginViewController : UIViewController<SocketIODelegate>
@property (nonatomic,retain) SocketIO * socketIO;
@end
