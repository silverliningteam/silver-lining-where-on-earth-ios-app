//
//  HeaderTableViewCell.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import <UIKit/UIKit.h>

@interface HeaderTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel * headerTitle;
@end
