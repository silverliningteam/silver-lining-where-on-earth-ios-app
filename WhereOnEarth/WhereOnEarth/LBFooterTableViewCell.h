//
//  LBFooterTableViewCell.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/13/14.
//
//

#import <UIKit/UIKit.h>

@interface LBFooterTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIButton * earnMoreButton;
@end
