//
//  WOEAnswerView.h
//  WhereOnEarth
//
//  Created by Phuc Tran Duy on 4/12/14.
//
//

#import "WOEBaseView.h"

@interface WOEAnswerView : WOEBaseView
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UIButton *countryBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@end
